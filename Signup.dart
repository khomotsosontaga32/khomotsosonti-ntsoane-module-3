import 'package:flutter/material.dart';
import 'package:flutter_application_1/Dashbord.dart';
import 'package:flutter_application_1/others/login.dart';
import 'package:google_fonts/google_fonts.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignupState();
}

class _SignupState extends State<SignUp> {
  final _emailController = TextEditingController();
  final fNameController = TextEditingController();
  final sNameController = TextEditingController();
  String password = "";
  String confpassword = "";
  bool isPassVisible = true;
  @override
  void initState() {
    super.initState();
    _emailController.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 226, 226, 226),
        body: SafeArea(
            child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.alarm,
                  size: 150,
                ),
                SizedBox(
                  height: 50,
                ),
                //hello
                Text(
                  "Welcome!",
                  style: GoogleFonts.bebasNeue(
                    fontSize: 55,
                  ),
                ),
                SizedBox(
                  height: 2.3,
                ),
                Text(
                  "Create a free account below",
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                //first name
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Color.fromARGB(255, 255, 255, 255)),
                        borderRadius: BorderRadius.circular(13),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(96, 255, 0, 0)),
                        borderRadius: BorderRadius.circular(13),
                      ),
                      labelText: "First Name",
                      hintStyle: TextStyle(
                        color: Color.fromARGB(198, 182, 182, 182),
                      ),
                      prefix: Icon(Icons.person),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                //last name
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Color.fromARGB(255, 255, 255, 255)),
                        borderRadius: BorderRadius.circular(13),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(96, 255, 0, 0)),
                        borderRadius: BorderRadius.circular(13),
                      ),
                      labelText: "Last Name",
                      hintStyle: TextStyle(
                        color: Color.fromARGB(198, 182, 182, 182),
                      ),
                      prefix: Icon(Icons.person),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                //phone number
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Color.fromARGB(255, 255, 255, 255)),
                        borderRadius: BorderRadius.circular(13),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromARGB(96, 255, 0, 0)),
                        borderRadius: BorderRadius.circular(13),
                      ),
                      labelText: "Phone Number",
                      hintStyle: TextStyle(
                        color: Color.fromARGB(198, 182, 182, 182),
                      ),
                      prefix: Icon(Icons.phone),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                //email field
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    controller: _emailController,
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Color.fromARGB(255, 255, 255, 255)),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color.fromARGB(96, 255, 0, 0)),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        labelText: "Email",
                        hintStyle: TextStyle(
                          color: Color.fromARGB(198, 182, 182, 182),
                        ),
                        prefix: Icon(Icons.mail),
                        suffixIcon: _emailController.text.isEmpty
                            ? Container(width: 0)
                            : IconButton(
                                icon: Icon(Icons.close),
                                onPressed: () => _emailController.clear(),
                              )),
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.done,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),

                //password field
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    onChanged: (value) => setState(() => this.password = value),
                    onSubmitted: (value) =>
                        setState(() => this.password = value),
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Color.fromARGB(255, 255, 255, 255)),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color.fromARGB(96, 255, 0, 0)),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        hintText: "Password",
                        suffixIcon: IconButton(
                          icon: isPassVisible
                              ? Icon(Icons.visibility_off)
                              : Icon(Icons.visibility),
                          onPressed: () =>
                              setState(() => isPassVisible = !isPassVisible),
                        ),
                        hintStyle: TextStyle(
                            color: Color.fromARGB(200, 161, 161, 161))),
                    obscureText: isPassVisible,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),

                //confirm password field
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    onChanged: (value) =>
                        setState(() => this.confpassword = value),
                    onSubmitted: (value) =>
                        setState(() => this.confpassword = value),
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Color.fromARGB(255, 255, 255, 255)),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color.fromARGB(96, 255, 0, 0)),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        hintText: "Confirm Password",
                        suffixIcon: IconButton(
                          icon: isPassVisible
                              ? Icon(Icons.visibility_off)
                              : Icon(Icons.visibility),
                          onPressed: () =>
                              setState(() => isPassVisible = !isPassVisible),
                        ),
                        hintStyle: TextStyle(
                            color: Color.fromARGB(200, 161, 161, 161))),
                    obscureText: isPassVisible,
                  ),
                ),

                SizedBox(
                  height: 10,
                ),
                //sign up button
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: GestureDetector(
                    onTap: () => {
                      print('Email is $_emailController'),
                      print('Password is $password'),
                      Navigator.pop(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const homePage())),
                    },
                    child: Container(
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: Color.fromARGB(146, 255, 0, 0),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Center(
                        child: Text(
                          "SignUp",
                          style: TextStyle(
                              color: Color.fromARGB(255, 255, 255, 255),
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                //not a member? registering now
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Already a member?",
                      style:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                    ),
                    GestureDetector(
                      onTap: () => {
                        Navigator.pop(context,
                            MaterialPageRoute(builder: (context) => login())),
                      },
                      child: Text(
                        " Login now",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic,
                            color: Color.fromARGB(127, 0, 140, 255),
                            fontSize: 12),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        )));
  }
}
