import 'package:flutter/material.dart';
import 'package:flutter_application_1/Dashbord.dart';
import 'package:flutter_application_1/others/Signup.dart';
import 'package:google_fonts/google_fonts.dart';
//import 'package:flutter_application_1/Signup.dart';

//Question1
class login extends StatefulWidget {
  @override
  State<login> createState() => _loginState();
}

class _loginState extends State<login> {
  //text controller
  final _emailController = TextEditingController();
  String password = "";
  bool isPassVisible = false;
  @override
  void initState() {
    super.initState();
    _emailController.addListener(() => setState(() {}));
  }

  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 226, 226, 226),
        body: SafeArea(
            child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.alarm,
                  size: 150,
                ),
                SizedBox(
                  height: 50,
                ),
                //hello
                Text(
                  "Welcome Back",
                  style: GoogleFonts.bebasNeue(
                    fontSize: 55,
                  ),
                ),
                SizedBox(
                  height: 2.3,
                ),
                Text(
                  "You have been missed you",
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                //email field

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    controller: _emailController,
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Color.fromARGB(255, 255, 255, 255)),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color.fromARGB(96, 255, 0, 0)),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        labelText: "Email",
                        hintStyle: TextStyle(
                          color: Color.fromARGB(198, 182, 182, 182),
                        ),
                        prefix: Icon(Icons.mail),
                        suffixIcon: _emailController.text.isEmpty
                            ? Container(width: 0)
                            : IconButton(
                                icon: Icon(Icons.close),
                                onPressed: () => _emailController.clear(),
                              )),
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.done,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                //password field
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: TextField(
                    onChanged: (value) => setState(() => this.password = value),
                    onSubmitted: (value) =>
                        setState(() => this.password = value),
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Color.fromARGB(255, 255, 255, 255)),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color.fromARGB(96, 255, 0, 0)),
                          borderRadius: BorderRadius.circular(13),
                        ),
                        hintText: "Password",
                        suffixIcon: IconButton(
                          icon: isPassVisible
                              ? Icon(Icons.visibility_off)
                              : Icon(Icons.visibility),
                          onPressed: () =>
                              setState(() => isPassVisible = !isPassVisible),
                        ),
                        hintStyle: TextStyle(
                            color: Color.fromARGB(200, 161, 161, 161))),
                    obscureText: isPassVisible,
                  ),
                ),

                SizedBox(
                  height: 10,
                ),
                //sign in button
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: GestureDetector(
                    onTap: () => {
                      print('Email is $_emailController'),
                      print('Password is $password'),
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => homePage())),
                    },
                    child: Container(
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: Color.fromARGB(146, 255, 0, 0),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: Center(
                        child: Text(
                          "Sign In",
                          style: TextStyle(
                              color: Color.fromARGB(255, 255, 255, 255),
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                //not a member? registering now
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Not a member?",
                      style:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                    ),
                    GestureDetector(
                      onTap: () => {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => SignUp())),
                      },
                      child: Text(
                        " Try making an account",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic,
                            color: Color.fromARGB(127, 0, 140, 255),
                            fontSize: 12),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        )));
  }
}
