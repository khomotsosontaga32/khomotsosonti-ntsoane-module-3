import 'package:flutter/material.dart';

class homePage extends StatefulWidget {
  const homePage({Key? key}) : super(key: key);

  @override
  State<homePage> createState() => _homePageState();
}

class _homePageState extends State<homePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromARGB(164, 240, 5, 25),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: Column(
              children: [
                //user info
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //hi user
                    Column(
                      children: [
                        Text(
                          "Hi, User!",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 255, 255, 255),
                              fontSize: 20),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          '10 May-2021',
                          style: TextStyle(
                              color: Colors.red[200],
                              fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),

                    Container(
                      decoration: BoxDecoration(
                          color: Color.fromARGB(255, 128, 12, 4),
                          borderRadius: BorderRadius.circular(6)),
                      padding: EdgeInsets.all(5),
                      child: Icon(Icons.notifications,
                          color: Color.fromARGB(255, 255, 255, 255)),
                    )
                  ],
                ),
                SizedBox(
                  height: 25,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Color.fromARGB(255, 128, 12, 4),
                    borderRadius: BorderRadius.circular(6),
                  ),
                  padding: EdgeInsets.all(5),
                  child: Row(
                    children: [
                      Icon(
                        Icons.search,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text('Search', style: TextStyle(color: Colors.white)),
                    ],
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                // choose zodiac sign
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Choose your zodiac sign",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 255, 255, 255),
                          fontSize: 15),
                    ),
                    Icon(
                      Icons.more_horiz,
                      color: Colors.white,
                    ),
                  ],
                ),
                SizedBox(
                  height: 25,
                ),
                Row(children: [
                  //4 different signs
                  //libra
                  homePage(),
                  //scopio

                  //germini

                  //Sagitorias
                ]),
              ],
            ),
          ),
        ));
  }
}
