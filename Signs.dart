import 'package:flutter/material.dart';

class zodiac extends StatelessWidget {
  const zodiac({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.red[900], borderRadius: BorderRadius.circular(5)),
      padding: EdgeInsets.all(6),
      child: Center(
        child: Text('♎'),
      ),
    );
  }
}
